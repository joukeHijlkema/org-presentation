function myLoadSlide() {
    var target = document.getElementsByClassName("slide")[0];
    var title  = target.hasAttribute("data-title") ? target.getAttribute("data-title"):"";
    var sTitle = target.hasAttribute("data-subtitle") ? target.getAttribute("data-subtitle"):"";
    var number = target.hasAttribute("data-number") ? target.getAttribute("data-number"):"";
    document.getElementById("slideTitle").innerHTML= title;
    document.getElementById("slideSubTitle").innerHTML= sTitle;
    document.getElementById("slideNumber").innerHTML= number;
    // console.log(title);
    // console.log("loaded");
}
