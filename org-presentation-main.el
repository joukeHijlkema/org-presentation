;;|--------------------------------------------------------------
;;|Description : pop up a help window
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 22-11-2020 14:11:21
;;|--------------------------------------------------------------
(defun opm-help ()
  "pop up a help window"
  (interactive)
  (let ((txt "Org Resentation Mode help:
General:
  - use the printPdf.sh script to transform your presentation to pdf (this is for brave, adapt for other browsers)
Latex:
  - \\begin{ causes trouble. Use \\beg{, the tool does a substitution"))
    (message-box txt)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Main entrance. run this to create the presentation
;;|NOTE : 
;;|- I started using straight so I modified the templateDir management
;;|Author : jouke hylkema
;;|date   : 05-29-2019 10:29:32
;;|--------------------------------------------------------------
(defun opm-runPresentationTool ()
  (interactive)
  (save-excursion
    ;; (message "this runs the presentation tool")
    (make-local-variable 'Params)
    (setq-local templateDir (s-replace "build" "repos"
                                       (file-name-directory (locate-library "org-presentation-mode"))))
    (setq-local imgCounter 0)
    (setq-local vidCounter 0)
    (setq-local arrowCounter 0)
    (setq-local slideCounter 0)
    (setq-local itemCounter 0)
    (setq-local X 0)
    (setq-local Y 0)
    (goto-char 0)
    (org-map-entries 'opm-Index)
    (org-element-map (org-element-parse-buffer) 'headline 'opm-doHeadline)
    (opm-savePresentation)
    );; (org-map-entries 'opm-doSlide "+Slide" 'file)
    ;; (message "%s" xmlTree)
    
  )
;;|--------------------------------------------------------------
;;|Description : index all headlines
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-19-2019 17:19:13
;;|--------------------------------------------------------------
(defun opm-Index ()
  "index all head lines with a unique id"
  (cond ((equal (list "Slide") (org-get-tags)) 
         (setq slideCounter (+ slideCounter 1))
         (org-entry-put (point) "id" (format "Slide_%d" slideCounter))
         (let* ((old (org-get-heading t t t t))
                (new (if (string-match-p "#" old)
                         (format "%s # %s" slideCounter (nth 1 (split-string old " # ")))
                       (format "%s # %s" slideCounter old)
                       )
                     )
                )
           (org-edit-headline new)
           )
         )
        (t
         (org-entry-put (point) "id" (format "item_%d" itemCounter))
         (setq itemCounter (+ itemCounter 1))
         )
        )
  )
(defun opm-savePresentation ()
  "save the presentation in the root. Part of this comes from esxml, we need end tags"
  ;; (message "%s" xmlTree)
  (let* ((xmlStr (opm-my-esxml-to-xml xmlTree)))
    (with-temp-file (f-join (opm-getParam "ROOT") "presentation.html")
      (insert xmlStr)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : transform the tree into a string
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-36-2019 16:36:16
;;|--------------------------------------------------------------
(defun opm-my-esxml-to-xml (esxml)
  "My version of pp-esxml-to-xml to assure we have end tags"
  ;; (message "%s" esxml)
  (cond ((stringp esxml) esxml)
        ((and (listp esxml)
              (> (length esxml) 1))
         (pcase-let ((`(,tag ,attrs . ,body) esxml))
           (cl-check-type tag symbol)
           (cl-check-type attrs attrs)
           (concat "<" (symbol-name tag)
                   (when attrs
                     (concat " " (mapconcat 'esxml--convert-pair attrs " ")))
                   ">"
                   (if body
                       (concat (if (cl-every 'stringp body)
                                   (mapconcat 'identity body " ")
                                 (concat "\n"
                                         (replace-regexp-in-string
                                          "^" "  "
                                          (mapconcat 'opm-my-esxml-to-xml body "\n"))
                                         "\n"))
                               ))
                   "</" (symbol-name tag) ">")))
        (t (error "%s is not a valid esxml expression !!" esxml)))
  )
;;|--------------------------------------------------------------
;;|Description : get a parameter
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 05-44-2019 15:44:51
;;|--------------------------------------------------------------
(defun opm-getParam (P &optional ret)
  "return the value of Param p or ret if not there"
  (let ((item (assoc P Params)))
    (if item (cdr item) ret)
    )
  )
;;|--------------------------------------------------------------
;;|Description : get a string from a file
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 05-45-2019 15:45:08
;;|--------------------------------------------------------------
(defun opm-getStringFromFile (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string))
  )
;;|--------------------------------------------------------------
;;|Description : get an xml node from a file
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 05-03-2019 15:03:19
;;|--------------------------------------------------------------
(defun opm-getHtmlNodeFromFile (filePath)
  "read the content of the file as an dom-node"
  (with-temp-buffer
    (insert-file-contents filePath)
    (libxml-parse-xml-region (point-min) (point-max))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Parse the parameters of the presentation
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-43-2019 14:43:30
;;|--------------------------------------------------------------
(defun opm-doParams (elt)
  ;; (message "== parse parameters ===")
  (setq Params (org-entry-properties))

  ;; === clean up if needed ===
  (when (equal (opm-getParam "KEEPOLDIMAGES") "False")
    (delete-directory "IMAGES" t)
    (delete-directory "VIDEOS" t)
    )
  ;; === make the directories ===
  (dolist (d '("CSS" "JS" "IMAGES" "VIDEOS"))
    (mkdir (f-join (opm-getParam "ROOT") d) t)
    )

  ;; === copy js and css ===
  (dolist (f '("impress.js"
               "myImpress.js"
               "vector.js"
               "pdf.js"))
    (copy-file (f-join templateDir "javaScript" f)
               (f-join (opm-getParam "ROOT") "JS" f)
               t)
    )
  (dolist (f '("Init.css"
               "Positioning.css"
               "PresentationBase.css"
               "Pdf.css"))
    (copy-file (f-join templateDir "CSS" f)
               (f-join (opm-getParam "ROOT") "CSS" f)
               t)
    )
  ;; === copy tools ===
  (copy-file (f-join templateDir "printPdf.sh")
             (f-join (opm-getParam "ROOT") "printPdf.sh")
             t)
  
  (if (and (opm-getParam "CSS") (> 0 (string-width (opm-getParam "CSS"))))
      (copy-file (f-join default-directory (opm-getParam "CSS"))
                 (f-join (opm-getParam "CSS") "PresentationExtra.css")
                 t)
    
    )
  ;; === Copy backgrounds ===
  (dolist (f '("Grid.svg"))
    (copy-file (f-join templateDir "CSS" "Backgrounds" f)
               (f-join (opm-getParam "ROOT") "IMAGES" f)
               t)
    )
  ;; (message "BG %s (%s)" (opm-getParam "BACKGROUNDS") (type-of (read (opm-getParam "BACKGROUNDS"))))
  (cl-loop for (target . source) in (read (opm-getParam "BACKGROUNDS")) do
           ;; (message "%s -> %s" source (format "%s.%s" target (f-ext source)))
           (opm-cpImage source (opm-getParam "SLIDEWIDTH") (opm-getParam "SLIDEHEIGHT")
                        (format "%s.%s" target (f-ext source)))  
           )
  (setq xmlTree
        (let ((xmlStr (opm-getStringFromFile (format "%sTemplates/presentation.html" templateDir))))
          (with-temp-buffer
            (insert xmlStr)
            (libxml-parse-html-region (point-min) (point-max))
            )
          )
        )
  
  (opm-setText (dom-by-id xmlTree "^mainTitle$") (opm-getParam "TITLE"))
  (if (opm-getParam "SUBTITLE")
      (opm-setText (dom-by-id xmlTree "^subTitle$") (opm-getParam "SUBTITLE")))
  (opm-setText (dom-by-id xmlTree "^Author$") (opm-getParam "AUTHOR"))
  (let* ((footer-text (dom-node 'span))
         (footer-date (dom-node 'span))
         (footer-main (dom-by-id xmlTree "^footer$")))
    (opm-setText footer-text (opm-getParam "FOOTERTITLE"))
    (dom-set-attribute footer-text 'id "footerTitle")
    (opm-setText footer-date (opm-getParam "FOOTERDATE"))
    (dom-set-attribute footer-date 'id "footerDate")
    (dom-append-child footer-main footer-date)
    (dom-append-child footer-main footer-text)
    )

  ;; set the dimensions
  (let* ((head (dom-by-tag xmlTree 'head))
         (dim (dom-node 'style)))
      (dom-append-child dim (format ".slide  {width:%spx;height:%spx}"
                                    (opm-getParam "SLIDEWIDTH") (opm-getParam "SLIDEHEIGHT")))
      (dom-append-child head dim))

  (opm-doLogos)

  ;; (message "=== DONE ===")
  )
;;|--------------------------------------------------------------
;;|Description : set the text of a node
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-19-2019 14:19:11
;;|--------------------------------------------------------------
(defun opm-setText (node txt)
  "set the text of this node"
  ;; (message "node: %s" node)
  (dolist (c (dom-children node))
    ;; (message "type: %s" (type-of c))
    (when (stringp c)
      (dom-remove-node xmlTree c)
      )
    )
  (dom-append-child node txt)
  )
;;|--------------------------------------------------------------
;;|Description : treat the logos
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-01-2019 15:01:38
;;|--------------------------------------------------------------
(defun opm-doLogos ()
  "insert all the logos"
  ;; (message "%s" (car (read-from-string (opm-getParam "LOGOS"))))
  ;; (message (dom-by-id xmlTree"^Root$"))
  (let ((logos (car (read-from-string (opm-getParam "LOGOS"))))
        (root (dom-by-id xmlTree "^Root$"))
        )
    (dolist (l logos)
      (let* ((s (opm-cpImage (cdr l) 0 0 nil t))
             (i (dom-node 'img (list (cons 'src s))) )
             (d (dom-node 'div (list (cons 'id (car l))) i)))
        (progn
          ;; (message "%s" root)
          ;; (message "%s" i)
          ;; (message "%s" d)
          (dom-append-child root d)
          )
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : treat a headline
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 05-05-2019 16:05:03
;;|--------------------------------------------------------------
(defun opm-doHeadline (elt)
  "treat a headline"
  ;; (message "====== Found a headline ======")
  (let* ((type (nth 0 (org-element-property :tags elt))))
    (cond
     ((equal type "Parameters") (opm-doParams elt))
     ((equal type "Slide") (opm-itemSlide elt))
     ((equal type "blist") (opm-itemBlist elt))
     ((equal type "box") (opm-itemBox elt))
     ((equal type "latex") (opm-itemLatex elt))
     ((equal type "video") (opm-itemVideo elt))
     ((equal type "image") (opm-itemImage elt))
     ((equal type "pdf") (opm-itemPdf elt))
     ((equal type "arrow") (opm-itemArrow elt))
     ((equal type "text") (opm-itemText elt))
     ((equal type "table") (opm-itemTable elt))
     (t (message "Don't know what to do with %s" type))
     )
    )
  )
;;|--------------------------------------------------------------
;;|Description : copy an image to the IMAGE dir and return the path
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-17-2019 15:17:20
;;|--------------------------------------------------------------
(defun opm-cpImage (source W H &optional filename noResize force)
  (setq imgCounter (+ imgCounter 1))
  (let* ((source (replace-regexp-in-string "#templates#" templateDir source)) 
         (ext (file-name-extension source))
         (targetName (or filename (format "img_%d.%s" imgCounter ext)))
         (target (f-join (opm-getParam "ROOT") "IMAGES" targetName))
         (cmd (if noResize
                  (format "convert %s %s" source target)
                (format "convert -resize %sx%s %s %s" W H source target)))
         )
    (progn
      ;; (message "targetName = %s" targetName)
      ;; (message "extension = %s" ext)
      ;; (message "source = %s,target = %s" source target)
      ;; (message "ext type = %s" (type-of ext))
      (unless (or force
                  (and (equal (opm-getParam "KEEPOLDIMAGES") "True") (file-exists-p target)))
        (pcase ext
          ("svg" (progn (copy-file source target t) (message "svg done")))
          ("pdf" (copy-file source target t))
          ;; ('pdf (shell-command (format "pdf2svg %s %s" source target)))
          (default (shell-command cmd)))
        )
      (format "IMAGES/%s" targetName)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : Copy a videoimage to the VIDEO directory
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-03-2019 11:03:47
;;|--------------------------------------------------------------
(defun opm-cpVideo (source id)
  "Copy a video to the VIDEOS directory"
  (setq vidCounter (+ vidCounter 1))
  (let* ((source (replace-regexp-in-string "#templates#" templateDir source)) 
         (ext (file-name-extension source))
         (targetName (format "vid_%s.%s" id ext))
         (target (f-join (opm-getParam "ROOT") "VIDEOS" targetName)))
    (copy-file source target t)
    (format "VIDEOS/%s" targetName)
    )
  )
;;|--------------------------------------------------------------
;;|Description : get a value from an alist where the keys are strings
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-42-2019 13:42:18
;;|--------------------------------------------------------------
(defun opm-get (key list)
  "get a value from an alist. No tests!!! I assume that key is a string"
  (alist-get key list nil nil'equal)
  )

;;|--------------------------------------------------------------
;;|Description : add things to a given attribute
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-22-2019 18:22:52
;;|--------------------------------------------------------------
(defun opm-addAttr (node where val sep)
  "add to class or style attributes"
  (let* ((oldAttr (alist-get where (dom-attributes node)))
         (newAttr (if oldAttr (format "%s%s%s" oldAttr sep val) val)))
    (dom-set-attribute node where newAttr)
    )
  )
;;|--------------------------------------------------------------
;;|Description : get the contents from an elements kids
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-26-2019 10:26:43
;;|--------------------------------------------------------------
(defun opm-getCont (elt type)
  "loop over the kids of type 'type and return their contents"
  (let ((items (org-element-map elt type
                  (lambda (i)
                    (let* ((start (org-element-property :contents-begin i))
                           (end (org-element-property :contents-end i)))
                           (buffer-substring start end)
                           )))))
    ;; (message "=== CONTENT: %s ===" items)
    items)
  )
;;|--------------------------------------------------------------
;;|Description : return the X coordinate of the slide
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-49-2019 15:49:37
;;|--------------------------------------------------------------
(defun opm-getX (elt &optional return)
  "return the X coordinate of the slide"
  (let* ((cmd (org-element-property :MOVEX elt))
         (factor (if cmd (string-to-number cmd) 1.5))
         (dx (* factor (string-to-number (opm-getParam "SLIDEWIDTH") )))
         (newX (if (cl-search "@" cmd)
                   (string-to-number cmd)
                 (+ X dx)
                 )
               )
         )
    ;; (message "cmd: %s, factor: %s, dx: %s, X: %s newX: %s" cmd factor dx X newX)
    (unless return (setq X newX))
    (format "%s" newX)
    )
  )
  
;;|--------------------------------------------------------------
;;|Description : return the Y coordinate of the slide
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-49-2019 15:49:37
;;|--------------------------------------------------------------
(defun opm-getY (elt &optional return)
  "return the Y coordinate of the slide"
  (let* ((cmd (org-element-property :MOVEY elt))
         (sh (string-to-number (opm-getParam "SLIDEHEIGHT")))
         (dx (cond ((string= cmd "top") (- Y))
                   ((string= cmd "down") (* 1.5 sh))
                   ((string= cmd "up") (- (* 1.5 sh)))
                   (t (* (- (if cmd (string-to-number cmd) 1.5)) sh))
                   )
                 )
         (newY (+ Y dx))
         )
    ;; (message "cmd: %s, factor: %s, dx: %s, X: %s newX: %s" cmd factor dy Y newY)
    (unless return (setq Y newY))
    (format "%s" newY)
    )
  )

;;|--------------------------------------------------------------
;;|Description : evaluate a string and return the value as a string
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-38-2019 16:38:45
;;|--------------------------------------------------------------
(defun opm-evalString (str)
  "evaluate a string and return the value as a string"
  (format "%s" (eval (read-from-string str)))
  )
;;|--------------------------------------------------------------
;;|Description : get an id from the handle propertie
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-31-2019 10:31:10
;;|--------------------------------------------------------------
(defun opm-getIdFromHandle (handle)
  "get an id from the handle propertie"
  (let ((found (org-element-map (org-element-parse-buffer) 'headline
                 (lambda (elt)
                   (if (equal (org-element-property :HANDLE elt) handle)
                       (org-element-property :ID elt)
                     nil)
                   )
                 )))
    ;; (message "found id=%s for handle %s" found handle)
    (nth 0 found)
    )
  )

(provide 'org-presentation-main)
