var Scale       = 1.0
var iniW        = parseInt(document.styleSheets[4].cssRules[0].style.width);
var iniH        = parseInt(document.styleSheets[4].cssRules[0].style.height);
var iniF        = 200; //Not pretty
var activeSlide = null;

// Wait for impress.js to be initialized
document.addEventListener( "impress:init", function( event ) {

    // Getting API from event data.
    // So you don't event need to know what is the id of the root element
    // or anything. `impress:init` event data gives you everything you
    // need to control the presentation that was just initialized.
    var api = event.detail.api;
    // Delegated handler for clicking on the links to presentation steps
    console.log("Start");
    document.addEventListener( "click", function( event ) {
	
	var element = event.target;
	console.log("clicked "+element);
        getRelativeClickCoords(event);
        
	if (element.hasAttribute("data-target") && !element.hasAttribute("data-block")) {
	    target = element.getAttribute("data-target")
	    // console.log(target)
            // If it's a link to presentation step, target this step
            if ( target && target[ 0 ] === "#" ) {
		target = document.getElementById( target.slice( 1 ) );
            }
	    // console.log("jump to "+target.tagName);
	    if ( api.goto( target ) ) {
		event.stopImmediatePropagation();
		event.preventDefault();
	    }
	}
    }, false );
});

// Actions to take when we enter a slide
document.addEventListener( "impress:stepenter", function(event){slideEnter(event.target)});
function slideEnter(target) {
    activeSlide = target;
    if (target.getElementsByTagName("video").length > 0) {
        // Start the video on enter
        var vids = target.getElementsByTagName("video");
        var i = 0;
        for (i=0;i<vids.length;i++) {
            vids.item(i).play();
        }
    }
    // load titles
    var title = target.hasAttribute("data-title") ? target.getAttribute("data-title"):"";
    var sTitle = target.hasAttribute("data-subtitle") ? target.getAttribute("data-subtitle"):"";
    var number = target.hasAttribute("data-number") ? target.getAttribute("data-number"):"";
    document.getElementById("slideTitle").innerHTML= title;
    document.getElementById("slideSubTitle").innerHTML= sTitle;
    document.getElementById("slideNumber").innerHTML= number;
    if (target.hasAttribute("data-position")) repositionItem(target);
    if (target.hasAttribute("data-links")) makeLinks(target.getAttribute("data-links"),target);
    if (target.hasAttribute("data-showFirstSubstep")) {
        console.log("open first substep");
        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('impress:substep:show', true, true);
        document.dispatchEvent( event );
    }
}

// Actions to take when we leave a slide
document.addEventListener( "impress:stepleave", function(event) {
    // console.log("stepleave");
    target=event.target;
    // pause videos
    if (target.getElementsByTagName("video").length > 0) {
        // Start the video on enter
        var vids = target.getElementsByTagName("video");
        var i = 0;
        for (i=0;i<vids.length;i++) {
            vids.item(i).pause();
        }
    }
    // remove titles
    document.getElementById("slideTitle").innerHTML= "";
    // console.log("done")
})
	
function makeLinks(links,slide) {
    console.log("=== link : "+links);
    var slideBB = target.getBoundingClientRect();
    var O      = new Vector(slideBB.x,slideBB.y);
    for (let pair of links.split(",")) {
	var elts   = pair.split(":");
	console.log("from "+elts[1]+" to "+elts[2]+". Width="+elts[3]+". Label="+elts[4]+
                    ". Color="+elts[5] + ". FSize="+elts[6] + ". dir="+elts[7] +
                    ". osStart="+elts[8] + ". osEnd="+elts[9]);
        var width = elts[3];
        var label = elts[4];
	var line = document.getElementById(elts[0]);
	var src = document.getElementById(elts[1]);
	var srcBB  = src.getBoundingClientRect();
	var srcAnc = elts[1].split(";")[1];
	var trg = document.getElementById(elts[2]);
	var trgBB  = trg.getBoundingClientRect();
	var trgAnc = elts[2].split(";")[1];
        var osStart = parseInt(elts[8])
        var osEnd = parseInt(elts[9])
        // console.log(typeof elts[8])
        if (typeof elts[5] === 'undefined' || elts[5] === 'nil') {
            var fill = "black";
        } else {
            var fill = elts[5]
        }
        if(typeof elts[6] === 'undefined' || elts[6] === 'nil') {
            var fsize = 45;
        } else {
            var fsize = elts[6]
        }
        

	P1 = new Vector(0.5*(srcBB.left+srcBB.right),0.5*(srcBB.bottom+srcBB.top));
	var dx1 = 0.5*srcBB.width+osStart;
	var dy1 = 0.5*srcBB.height+osStart;

	P2 = new Vector(0.5*(trgBB.left+trgBB.right),0.5*(trgBB.bottom+trgBB.top));
	var dx2 = 0.5*trgBB.width+osEnd;
	var dy2 = 0.5*trgBB.height+osEnd;

        P1.subtract(O);
        P2.subtract(O);

        P1n = intersect(P1,P2,dx1,dy1);
        P2n = intersect(P2,P1,dx2,dy2);

        if (elts[7] == "horizontal") {
            ny = 0.5*(P1n.y+P2n.y);
            P1n.y = ny;
            P2n.y = ny;
        } else if (elts[7] == "vertical") {
            nx = 0.5*(P1n.x+P2n.x);
            P1n.x = nx;
            P2n.x = nx;
        }
            
	line.setAttribute("d",arrowString(P1n,P2n,width,0.5*width));
	line.setAttribute("stroke", fill);  
	line.setAttribute("stroke-width", width);  
	line.setAttribute("fill", fill);

        if (elts[4] != "nil") {
            var labelPath = document.createElementNS('http://www.w3.org/2000/svg',"path");
            dP = Vector.subtract(P2n,P1n).normalize();
            nP = Vector.multiply(Vector.rotate(dP,1.5*Math.PI),0.9*width);
            // console.log("dP = "+dP.x+","+dP.y);
            // console.log("nP = "+nP.x+","+nP.y);
            S  = Vector.add(P1n,nP)
            E  = Vector.add(P2n,nP)
            labelPath.setAttributeNS(null, "id", "textPath_"+elts[0]);    
            labelPath.setAttributeNS(null, "d","M"+S.x+","+S.y+"L"+E.x+","+E.y);
            labelPath.setAttributeNS(null,"fill", "none");
            labelPath.setAttributeNS(null,"stroke","none");
            line.parentElement.appendChild(labelPath)
                
	    var label = document.createElementNS('http://www.w3.org/2000/svg',"text");
	    label.setAttributeNS(null,"font-size",1.5*width);
	    label.setAttributeNS(null,"fill",fill);
	    label.setAttributeNS(null,"text-anchor","start");
	    
            var textPath = document.createElementNS("http://www.w3.org/2000/svg","textPath");
            textPath.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "#textPath_"+elts[0]);
            textPath.setAttributeNS(null,"startOffset","10");
            textPath.setAttributeNS(null,"font-size",fsize);
            
	    var textNode = document.createTextNode(elts[4]);
            
            textPath.appendChild(textNode);
            label.appendChild(textPath);
            line.parentElement.appendChild(label);
	}	
    }
}

function intersect(P1,P2,dx,dy) {
    D = P1.clone();
    D.subtract(P2);
    
    if (D.x == 0) {
        l1 = -10;
        l2 = -10;
        l3 = (P1.y+dy-P2.y)/(P1.y-P2.y);
        l4 = (P1.y-dy-P2.y)/(P1.y-P2.y);
    } else if (D.y ==0) {
        l1 = (P1.x+dx-P2.x)/(P1.x-P2.x);
        l2 = (P1.x-dx-P2.x)/(P1.x-P2.x);
        l3 = -10;
        l4 = -10;
    } else {
        l1 =( P1.x+dx-P2.x)/(P1.x-P2.x);
        l2 =( P1.x-dx-P2.x)/(P1.x-P2.x);
        l3 = (P1.y+dy-P2.y)/(P1.y-P2.y);
        l4 = (P1.y-dy-P2.y)/(P1.y-P2.y);
    }
    l  = Math.max(Math.min(l1,l2),Math.min(l3,l4));
    // console.log("D="+D.x+","+D.y);
    // console.log("P1="+P1.x+","+P1.y);
    // console.log("P2="+P2.x+","+P2.y);
    // console.log("l1="+l1);
    // console.log("l2="+l2);
    // console.log("l3="+l3);
    // console.log("l4="+l4);
    // console.log("l="+l);
    // console.log("dx="+dx);
    // console.log("dy="+dy);
    return P1.clone().add(D.multiply(l-1));
}

    
function arrowString(P1,P2,l,w) {
    dP = Vector.subtract(P2,P1).normalize();
    nP = Vector.rotate(dP,0.5*Math.PI);
    P3 = Vector.subtract(P2,Vector.multiply(dP,2*l));
    P4 = Vector.add(P3,Vector.multiply(nP,w));
    P5 = Vector.subtract(P3,Vector.multiply(nP,w));
    // P2.subtract(dP.multiply(10));
    
    Pt = Vector.subtract(P2,Vector.multiply(dP,l));
    var d = "M"+P1.x+" "+P1.y;
    d+= "L "+P3.x+" "+P3.y;
    d+= "L "+P4.x+" "+P4.y;
    d+= "L "+Pt.x+" "+Pt.y;
    d+= "L "+P5.x+" "+P5.y;
    d+= "L "+P3.x+" "+P3.y;
    
    console.log(d);

    return d;
}

function getRelativeClickCoords(event) {
    console.log(event.target)
    if (event.target.id == "myResetScale") {
        setScale(1);
        changeScale.value=50;
    } else {
        try {
	    var parentSlide = event.target.closest(".slide");
	    var Px = parseFloat(parentSlide.getAttribute("data-x"));
	    var Py = parseFloat(parentSlide.getAttribute("data-y"));
	    var R  = parentSlide.getBoundingClientRect()
	    var Rx = R.left+0.5*(R.right-R.left);
	    var Ry = R.top+0.5*(R.bottom-R.top);
	    var Cx = event.clientX;
	    var Cy = event.clientY;
	    var dx = Cx-Rx;
	    var dy = Cy-Ry;
	    var x  = Px+dx;
	    var y  = Py+dy;

	    // console.log("Delta X = "+dx);
	    // console.log("Delta Y = "+dy);
	    // console.log("abs   X = "+x);
	    // console.log("abs   Y = "+y);
        } catch(error) {
	    console.log("could not get relative coordinates");
            console.log(error);
        }
    }
}

function setScale(S) {
    Scale = S;
    console.log("scale "+Scale);
    let steps = Array.from(document.getElementsByClassName("step"));
    steps.forEach(updatePositions,Scale);
    document.styleSheets[4].cssRules[0].style.width=`${Scale*iniW}px`;
    document.styleSheets[4].cssRules[0].style.height=`${Scale*iniH}px`;
    document.body.style.fontSize=`${Scale*iniF}%`;
    slideEnter(activeSlide);
}

function updatePositions(item) {
	// console.log("update scale for "+item+" with scale "+this);	
	var R = item.getBoundingClientRect();
	var Rx = R.left+0.5*(R.right-R.left);
	var Ry = R.top+0.5*(R.bottom-R.top);
	if (item.hasAttribute("addx")) {
		x  = parseFloat(item.getAttribute("data-x"));
		dx = parseFloat(item.getAttribute("addx"));
		nx = x-Scale*dx+this*dx;
		item.setAttribute("data-x",nx);
		// console.log(Scale+": x "+x+" -> "+nx)
	}
	if (item.hasAttribute("addy")) {
		y  = parseFloat(item.getAttribute("data-y"));
		dy = parseFloat(item.getAttribute("addy"));
		ny = y-Scale*dy+this*dy;
		item.setAttribute("data-y",ny);
		// console.log("dy="+dy);
		// console.log("Old scale = "+Scale);
		// console.log("New scale = "+this);
		// console.log("y: "+y+" -> "+ny)
	};
}

// --------------------------------------------------------------
// Description : position an item relative to another
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 08-10-2010 15:01:36
// --------------------------------------------------------------
async function repositionItem(slide) {
    console.log("== reposition ==");
    var positions = slide.getAttribute("data-position");
    for (let rep of positions.split(",")) {
        console.log("reposition " + rep);
        
        var elts      = rep.split(":")
        var pos       = elts[1];
        var targetElt = document.getElementById(elts[0]);
        var relative  = document.getElementById(elts[2]);
        targetElt.style.position = "absolute";
        var center   = parseFloat(elts[4]);

        switch (pos) {
        case "below":
            console.log("Below")
            var margin   = parseFloat(elts[3]);
            var BB1 = relative.getBoundingClientRect();
            var BB2 = targetElt.getBoundingClientRect();
            var BB3 = slide.getBoundingClientRect();

            console.log("X1="+BB1.x+" Y1="+BB1.y+" W1="+BB1.width+" H1="+BB1.height);
            console.log("X2="+BB2.x+" Y2="+BB2.y+" W2="+BB2.width+" H2="+BB2.height);
            console.log("X3="+BB3.x+" Y3="+BB3.y+" W3="+BB3.width+" H3="+BB3.height);
            console.log("Margin = "+margin);

            var x2 = Math.round(BB1.x -BB3.x + center*(BB1.width-BB2.width));
            targetElt.style.left = x2;
            console.log("x2="+BB1.x+"-"+BB3.x+"+0.5x("+BB1.width+"-"+BB2.width+") = "+x2);
            if (margin>0) {
                var y2 = Math.round(BB1.y - BB3.y + BB1.height + margin);
                console.log("y2 = "+BB1.y+"-"+BB3.y+"+"+BB1.height+"+"+margin+" = "+y2);
                targetElt.style.top  = y2;
            }
            break;
        case "left":
            var margin   = parseFloat(elts[3]);
            console.log(targetElt.id+" left of "+relative.id+" with a margin of "+margin);
            
            var BB1 = relative.getBoundingClientRect();
            var BB2 = targetElt.getBoundingClientRect();
            var BB3 = slide.getBoundingClientRect();

            console.log("X1="+BB1.x+" Y1="+BB1.y+" W1="+BB1.width+" H1="+BB1.height);
            console.log("X2="+BB2.x+" Y2="+BB2.y+" W2="+BB2.width+" H2="+BB2.height);
            console.log("X3="+BB3.x+" Y3="+BB3.y+" W3="+BB3.width+" H3="+BB3.height);
            console.log("Margin = "+margin);

            var x2 = Math.round(BB1.x - BB3.x - BB2.width - margin);
            var y2 = Math.round(BB1.y - BB3.y + center*(BB1.height - BB2.height));
            console.log("x2="+BB1.x+"-"+BB3.x+"-"+BB2.width+"-"+margin+" = "+x2);
            console.log("y2="+BB1.y+"-"+BB3.y+"+0.5x("+BB1.height+"-"+BB2.height+") = "+y2);
            targetElt.style.left = x2;
            targetElt.style.top  = y2;
           
            break;
        case "right":
            var margin   = parseFloat(elts[3]);
            console.log(targetElt.id+" right of "+relative.id+" with a margin of "+margin);
            
            var BB1 = relative.getBoundingClientRect();
            var BB2 = targetElt.getBoundingClientRect();
            var BB3 = slide.getBoundingClientRect();

            console.log("X1="+BB1.x+" Y1="+BB1.y+" W1="+BB1.width+" H1="+BB1.height);
            console.log("X2="+BB2.x+" Y2="+BB2.y+" W2="+BB2.width+" H2="+BB2.height);
            console.log("X3="+BB3.x+" Y3="+BB3.y+" W3="+BB3.width+" H3="+BB3.height);
            console.log("Margin = "+margin);
            console.log("Center = "+center);

            var x2 = Math.round(BB1.x + BB1.width + margin - BB3.x);
            var y2 = Math.round(BB1.y - BB3.y + center*(BB1.height - BB2.height));
            console.log("x2="+BB1.x+"+"+BB1.width+"+"+margin+"-"+BB3.x+" = "+x2);
            console.log("y2="+BB1.y+"-"+BB3.y+"+0.5x("+BB1.height+"-"+BB2.height+") = "+y2);
            targetElt.style.left = x2;
            targetElt.style.top  = y2;
           
            break;
        case "between":
            var relative2 = document.getElementById(elts[3]);
            var BB1 = relative.getBoundingClientRect();
            var BB2 = targetElt.getBoundingClientRect();
            var BB3 = relative2.getBoundingClientRect();
            var BB4 = slide.getBoundingClientRect();

            var Cx = 0.5*(BB1.x+0.5*BB1.width  + BB3.x+0.5*BB3.width)  - BB4.x;
            var Cy = 0.5*(BB1.y+0.5*BB1.height + BB3.y+0.5*BB3.height) - BB4.y;

            var x2 = Math.round(Cx - 0.5*BB2.width);
            targetElt.style.left = x2;
            var y2 = Math.round(Cy - 0.5*BB2.height);
            targetElt.style.top  = y2;

            console.log("X1="+BB1.x+" Y1="+BB1.y+" W1="+BB1.width+" H1="+BB1.height);
            console.log("X2="+BB2.x+" Y2="+BB2.y+" W2="+BB2.width+" H2="+BB2.height);
            console.log("X3="+BB3.x+" Y3="+BB3.y+" W3="+BB3.width+" H3="+BB3.height);
            console.log("X4="+BB4.x+" Y4="+BB4.y+" W4="+BB4.width+" H4="+BB4.height);
            console.log("Cx="+Cx+", Cy="+Cy);
            console.log("x2="+x2+", y2="+y2);
            break;
        }
        
    }
}

var changeScale = document.getElementById("myScale");
changeScale.oninput = function() {
    let sMin   = 0.1;
    let sMax   = 2.0;
    let a      = (1-sMin)/49;
    let b      = 1 - 50*a;
    setScale(a*this.value + b)
}
var resetScale = document.getElementById("myScale");
resetScale.oninput = setScale(1);
