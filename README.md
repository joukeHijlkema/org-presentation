# org-presentation

An impress.js and org-mode based html5 presentation generator 

## Quick start
### Emacs set-p
I use something like:


```
(use-package org-presentation-mode
    :mode ("\\.orgp\\'" . org-presentation-mode)
    :init
    (message "=== my init org presentation mode ===")
    :custom
    (message "=== my custom org presentation mode ===")
    :config
    (message "=== my config org presentation mode ===")
    :bind (:map org-presentation-mode-map
            ("<s-f12>"	.	opm-runPresentationTool)
          )
)   
```
### Usage
Read the Tests/test.orgp file. Modify to your taste and execute M-x opm-runPresentationTool
If all goes well you'll find a presentation.html in the root directory defined in test.orgp.
