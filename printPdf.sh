#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage : printPdf.sh number-of-slides"
    exit
fi
if ! command -v xdotool &> /dev/null
then
    echo "you need to install xdotool for this to work"
fi
if ! command -v import &> /dev/null
then
    echo "you need to install imagemagic (import) for this to work"
fi

rm -f /tmp/*.pdf
rm -f presentation.pdf

echo "Make sure the presentation is visible, full screen and on the first slide. Then hit return and don't touch anything untill finished"
read dummy
    
FFWID=$(xdotool search --name org-presentation | head -n1)
xdotool windowfocus "$FFWID"

for((s=1;s<=$1;s++))
do
    sleep 3
    import -window $FFWID $(printf "/tmp/P_%03d.pdf" $s)
    xdotool windowfocus "$FFWID"
    xdotool key "space"
done

pdfunite /tmp/P_*.pdf presentation.pdf
