;;|--------------------------------------------------------------
;;|Description : generic item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-00-2019 12:00:42
;;|--------------------------------------------------------------
(defun opm-item (elt type &optional class)
  "generic actions for an item"
  ;; (message "=== opm-item %s ===" type)
  (let* ((id (or (org-element-property :ID elt) "-"))
         (parent (org-element-property :parent elt))
         (parentId (org-element-property :ID parent))
         (parentNode (dom-by-id xmlTree (format "^%s$" parentId)))
         (itemNode (if class (dom-node type `((class . ,class)))
                     (dom-node type)))
         (attributes '(("class" . " ") ("style" . ",")))
         (pos (org-element-property :POSITION elt))
         )
    ;; (message "DEBUG id = %s, pos = %s" id pos)
    (dom-set-attribute itemNode 'id id)
    (if pos
        (let* ((oldPositions (dom-attr parentNode 'data-position))
               (elts (s-split ":" pos))
               (position (nth 0 elts))
               (relative (opm-getIdFromHandle (nth 1 elts)))
               (param (cond ((s-equals-p position "below")
                             (or (nth 2 elts) 0))
                            ((s-equals-p position "left")
                             ;; (message "elt: %s" elts)
                             (or (nth 2 elts) 0))
                            ((s-equals-p position "right")
                             (or (nth 2 elts) 0))
                            ((s-equals-p position "between")
                             (opm-getIdFromHandle (nth 2 elts)))
                            )
                      )
               (center (or (nth 3 elts) 0.5))
               (newPosition (s-join "," (cl-loop for p in
                                                 (list oldPositions (format "%s:%s:%s:%s:%s"
                                                                            id position relative param center))
                                                 if p collect p)))
               )
          ;; (message "new position %s" newPosition)
          (dom-set-attribute parentNode 'data-position newPosition)
          )
      )
    (dom-append-child parentNode itemNode)
    (dolist (attr attributes)
      (let* ((prop (intern (car attr)))
             (sep (cdr attr))
             (Prop (intern (format ":%s" (upcase (car attr))))))
        (when (org-element-property Prop elt)
          (opm-addAttr itemNode prop (org-element-property Prop elt) sep)
          )
        )
      )
    `(("id" . ,id) ("parent" . ,parent) ("parentNode" . ,parentNode) ("itemNode" . ,itemNode))
    )
  )
;;|--------------------------------------------------------------
;;|Description : parse a slide item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-26-2019 19:26:18
;;|--------------------------------------------------------------
(defun opm-itemSlide (elt)
    "return a slide item"
    ;; (message "=== parse slide %s ===" (org-element-property :raw-value elt))
    (setq slideCounter (+ slideCounter 1))
    (let* ((id (org-element-property :ID elt))
           (h (org-element-property :TITLE elt))
           (s (dom-node 'div `((class . ,(format "step slide %s" (org-element-property :CLASS elt))) (id . ,(format "%s" id)))))
           (r (dom-by-id xmlTree "^impress$"))
           )
      (dom-set-attribute s 'data-title h)
      (dom-set-attribute s 'data-number (format "%s" slideCounter))
      (dom-set-attribute s 'data-name h)
      ;; === Movement ===
      (dom-set-attribute s 'data-x (opm-getX elt (org-element-property :RETURN elt)))
      (dom-set-attribute s 'data-y (opm-getY elt (org-element-property :RETURN elt)))
      ;; === Rotation & scale ===
      (dom-set-attribute s 'data-scale (if (org-element-property :SCALE elt)
                                           (org-element-property :SCALE elt) "1.0"))
      (dom-set-attribute s 'data-rotate-z (if (org-element-property :ROTATEZ elt)
                                           (org-element-property :ROTATEZ elt) "0.0"))
      (dom-set-attribute s 'data-rotate-y (if (org-element-property :ROTATEY elt)
                                           (org-element-property :ROTATEY elt) "0.0"))
      (dom-set-attribute s 'data-rotate-x (if (org-element-property :ROTATEX elt)
                                              (org-element-property :ROTATEX elt) "0.0"))
      (when (org-element-property :SHOWFIRSTSUBSTEP elt)
        (dom-set-attribute s 'data-showFirstSubstep "t"))
      (when (or (opm-getParam "DRAFT") (org-element-property :BACKGROUND elt))
        (let* ((src1 (if (org-element-property :BACKGROUND elt)
                       (opm-cpImage (org-element-property :BACKGROUND elt)
                                    (opm-getParam "SLIDEWIDTH") (opm-getParam "SLIDEHEIGHT"))
                       nil))
               (src2 (if (opm-getParam "DRAFT") "IMAGES/Grid.svg" nil))
               (bg (concat "background-size:contain;background-blend-mode: overlay;background-image:"
                           (if src1 (format "url(%s)" src1))
                           (if (and src1 src2) ",")
                           (if src2 (format "url(%s)" src2)))))
          (dom-set-attribute s 'style bg)
          )
        )
      (when (org-element-property :SUBTITLE elt)
        (dom-set-attribute s 'data-subtitle (org-element-property :SUBTITLE elt)))
      (when (org-element-property :OUTLINE elt)
        (opm-itemOutline s))
      (dom-append-child r s)
      (when (org-element-property :RETURN elt) (opm-duplicate-slide r (org-element-property :RETURN elt) id))
      )
    )
;;|--------------------------------------------------------------
;;|Description : duplicate slide with handle H
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-52-2021 18:52:39
;;|--------------------------------------------------------------
(defun opm-duplicate-slide (r H from)
  "duplicate slide with handle H"
  (let* ((target (nth 0 (org-element-map (org-element-parse-buffer) 'headline
                         (lambda (h)
                           (if (string= (org-element-property :HANDLE h) H)
                               (org-element-property :ID h)
                             nil)
                           ))))
        (targetNode (nth 0 (dom-by-id r target)))
        )
    ;; (message "handle %s -> id: %s (%s)" H target (type-of targetNode))
    (dom-set-attribute targetNode `id (format "item_5_from_%s" id))
    (dom-append-child r targetNode)
    )
  )
;;|--------------------------------------------------------------
;;|Description : create an outline
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-51-2021 10:51:58
;;|--------------------------------------------------------------
(defun opm-itemOutline (targetSlide)
  "create an outline in targetSlide"
  (interactive)
  (let ((titles (org-element-map (org-element-parse-buffer) 'headline
                  (lambda (h)
                    (if (org-element-property :INOUTLINE h)
                        (org-element-property :TITLE h)
                      nil
                      )
                    )
                  ))
        (outline (dom-node 'ul))
        )
    ;; (message "found titles: %s" titles)
    (cl-loop for T in titles do
             (let ((li (dom-node 'li)))
               (dom-append-child li (opm-format T))
               (dom-append-child outline li)
               )
             )
    (dom-append-child targetSlide outline)
    )
  )
;;|--------------------------------------------------------------
;;|Description : parse a table item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-58-2023 13:58:07
;;|--------------------------------------------------------------
(defun opm-itemTable (elt)
  "parse a table item"
  (let* ((data (opm-item elt 'div "Table"))
         (s (org-element-property :contents-begin elt))
         (e (org-element-property :contents-end elt))
         (raw (substring (buffer-substring s e)))
         (txt (opm-format (substring raw (+ 5 (string-match ":END:" raw)))))
         (title (org-element-property :TITLE elt))
         (node (with-temp-buffer
                 (insert txt)
                 (goto-line 2)
                 (org-table-export "/tmp/Table.html" "orgtbl-to-html")
                 (erase-buffer)
                 (find-file "/tmp/Table.html")
                 (nth 0 (dom-by-tag (libxml-parse-html-region (point-min) (point-max )) 'table))
                 )
               )
         )
    (when title
      (let* ((cap (dom-node 'caption)))
        (dom-append-child cap title)
        (dom-append-child node cap)
        )
      )
    (dom-append-child (opm-get "itemNode" data) node)
    (kill-buffer "Table.html")
    )
  )
;;|--------------------------------------------------------------
;;|Description : parse a blist
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-32-2019 19:32:32
;;|--------------------------------------------------------------
(defun opm-itemBlist (elt)
  "return a blist item"

  (let* ((myList (opm-get "itemNode" (opm-item elt 'ul "bList"))))
    (org-element-map (org-element-contents elt) 'item
      (lambda (i)
        (dolist (j (org-element-contents i))
          (cond
           ((equal (org-element-type j) 'paragraph)
            ;; (message "JOUKE: j=%s" (last j))
            (let* ((li (dom-node 'li))
                   (line (car (last j)))
                   (txt (nth 0 (split-string line " #")))
                   (step (nth 1 (split-string line " #"))))
              (dom-append-child li (opm-format txt))
              (if step (dom-set-attribute li 'class "substep"))
              (dom-append-child myList li)
              )
            )
           ((equal (org-element-type j) 'plain-list)
            (dom-append-child myList (opm-itemBlist j)))
           )
          )
        )
      nil nil 'item nil)
    myList
    )
  )
;;|--------------------------------------------------------------
;;|Description : parse a box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-59-2019 11:59:40
;;|--------------------------------------------------------------
(defun opm-itemBox (elt)
  "create a box"
  (let* ((data (opm-item elt 'div "Box")))
    ;; (message "=== a box ===")
    (when (org-element-property :TITLE elt)
      (let* ((titleNode (dom-node 'div (list (cons 'class "boxTitle")))))
        (dom-append-child (opm-get "itemNode" data) titleNode)
        (dom-append-child titleNode  (org-element-property :TITLE elt))
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : a text item
;;|NOTE : this goes in a div
;;|-
;;|Author : jouke hylkema
;;|date   : 11-13-2019 12:13:23
;;|--------------------------------------------------------------
(defun opm-itemText (elt)
  "a text item"
  (let* ((data (opm-item elt 'div "Text"))
         (tit (opm-format (org-element-property :raw-value elt) "%s<br>"))
         (s (org-element-property :contents-begin elt))
         (e (org-element-property :contents-end elt))
         (raw (substring (buffer-substring s e)))
         (txt (substring raw (+ 5 (string-match ":END:" raw))))
         )
    ;; (message "%s->%s %s" s e txt)
    (dom-append-child (opm-get "itemNode" data) (opm-format txt "<span>%s</span>"))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Parse a latex item
;;|NOTE : this uses external software
;;|- \begin is causing trouble. Not sure why. Use \beg{}, il replace it.
;;|Author : jouke hylkema
;;|date   : 06-26-2019 21:26:24
;;|--------------------------------------------------------------
(defun opm-itemLatex (elt)
  "Parse a latex item, construct a file and run it through pdflatex"
  ;; (message "=== Latex block ===")
  (save-window-excursion
    (let* ((data (opm-item elt 'div "latex"))
           (fileNameBase (format "eq_%s" (opm-get "id" data)))
           (formula (opm-getCont elt 'paragraph))
           (img (dom-node 'img))
           )
      (with-temp-file (format "/tmp/%s.tex" fileNameBase)
        (insert "\\documentclass[crop=true,border=1pt]{standalone}\n")
        (insert "\\batchmode\n")
        (insert "\\usepackage[utf8]{inputenc}\n")
        (insert "\\usepackage{amsmath}\n")
        (insert "\\usepackage[svgnames]{xcolor}\n")
        (insert "\\makeatletter\n")
        (insert "\\newcommand{\\myCol}[2]{\\color{#1} #2 \\color{Black}}\n")
        (insert "\\newcommand{\\Green}[1]{\\color{DarkGreen} #1 \\color{Black}}\n")
        (insert "\\newcommand{\\Red}[1]{\\color{Red} #1 \\color{Black}}\n")
        (insert "\\newcommand{\\Blue}[1]{\\color{Blue} #1 \\color{Black}}\n")
        (insert "\\newcommand{\\Orange}[1]{\\color{Orange} #1 \\color{Black}}\n")
        (insert "\\newcommand{\\White}[1]{\\color{White} #1 \\color{Black}}\n")
        (if (org-element-property :FONT elt)
            (insert (format "\\renewcommand{\\rmdefault}{%s}\n" (org-element-property :FONT elt))))
        (insert "\\makeatother\n")
        (insert "\\begin{document}\n")
        (if (not (org-element-property :EMPTY elt))
            (progn
              (insert "\\begin{math}\n")
              (insert " \\begin{aligned}\n")
              (insert (format "   %s" (replace-regexp-in-string "beg{" "begin{" (string-join formula "###"))))
              (insert " \\end{aligned}\n")
              (insert "\\end{math}\n")
              )
           (insert (format "   %s" (replace-regexp-in-string "beg{" "begin{" (string-join formula "###"))))
          )
        (insert "\\end{document}\n"))
      (shell-command (format "cd /tmp; pdflatex -shell-escape %s.tex" fileNameBase))
      (shell-command (format "pdf2svg /tmp/%s.pdf %s/IMAGES/%s.svg" fileNameBase (opm-getParam "ROOT") fileNameBase))
      (dom-set-attribute img 'src (format "IMAGES/%s.svg" fileNameBase))
      (dom-set-attribute img 'style "width:100%;")
      (dom-append-child (opm-get "itemNode" data) img)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : a movie item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-54-2019 10:54:13
;;|--------------------------------------------------------------
(defun opm-itemVideo (elt)
  "add a video item"
  (let* ((data (opm-item elt 'div "video"))
         (video (dom-node 'video))
         (base (org-element-property :BASE elt))
         (id (org-element-property :ID elt))
         (exts (car (read-from-string (org-element-property :EXTS elt)))))
    (dom-set-attribute video 'controls "controls")
    (dom-set-attribute video 'loop "loop")
    (dolist (e exts)
      (let* ((newSrc (opm-cpVideo (format "%s.%s" base e) id))
             (a (dom-node 'source)))
        (dom-set-attribute a 'src newSrc)
        (dom-set-attribute a 'type (format "video/%s" e))
        (dom-append-child video a)))
    (dom-append-child (opm-get "itemNode" data) video)
    )
  )
;;|--------------------------------------------------------------
;;|Description : add a pdf item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-43-2019 13:43:27
;;|--------------------------------------------------------------
(defun opm-itemPdf (elt)
  "add a pdf item"
  (let* ((data (opm-item elt 'figure "image"))
         (newFile (opm-cpImage (org-element-property :SRC elt)
                               (opm-getParam "SLIDEWIDTH")
                               (opm-getParam "SLIDEHEIGHT")))
         (img (dom-node 'embed)))
    (dom-set-attribute img 'src newFile)
    (dom-set-attribute img 'type "application/pdf")
    (dom-set-attribute img 'id (format "img_%d" imgCounter))
    (dom-set-attribute img 'style (format "height:inherit;" imgCounter))
    (when (org-element-property :CAPTION elt)
      (let ((cap (dom-node 'figcaption)))
        (dom-append-child cap (org-element-property :CAPTION elt))
        (dom-append-child (opm-get "itemNode" data) cap)
        )
      )
    (dom-append-child (opm-get "itemNode" data) img)
    )
  )
;;|--------------------------------------------------------------
;;|Description : add an image item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-43-2019 13:43:27
;;|--------------------------------------------------------------
(defun opm-itemImage (elt)
  "add an image item"
  (let* ((data (opm-item elt 'figure "image"))
         (id (org-element-property :ID elt))
         (target (format "img_%s.%s"
                         id
                         (file-name-extension (org-element-property :SRC elt))
                         )
                 )
         (newFile (opm-cpImage (org-element-property :SRC elt)
                               (opm-getParam "SLIDEWIDTH")
                               (opm-getParam "SLIDEHEIGHT")
                               target)
                  )
         (img (dom-node 'img)))
    (dom-set-attribute img 'src newFile)
    (dom-set-attribute img 'id (format "img_%s" id))
    (when (org-element-property :CAPTION elt)
      (let ((cap (dom-node 'figcaption)))
        (dom-append-child cap (org-element-property :CAPTION elt))
        (dom-append-child (opm-get "itemNode" data) cap)
        )
      )
    (dom-append-child (opm-get "itemNode" data) img)
    )
  )
;;|--------------------------------------------------------------
;;|Description : add an arrow
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-15-2019 14:15:10
;;|--------------------------------------------------------------
(defun opm-itemArrow (elt)
  "add an arrow"
  (setq arrowCounter (+ arrowCounter 1))
  (let* ((data (opm-item elt 'svg "arrow"))
         (id (format "arrow_%d" arrowCounter))
         (path (dom-node 'path))
         (parent (org-element-property :parent elt))
         (from (opm-getIdFromHandle (org-element-property :FROM elt)))
         (to (opm-getIdFromHandle (org-element-property :TO elt)))
         (width (org-element-property :WIDTH elt))
         (parentId (org-element-property :ID parent))
         (parentNode (dom-by-id xmlTree (format "^%s$" parentId)))
         (links (dom-attr parentNode 'data-links))
         (label (org-element-property :LABEL elt))
         (color (org-element-property :COLOR elt))
         (fsize (org-element-property :FSIZE elt))
         (dir   (org-element-property :DIR elt))
         (osStart (or (org-element-property :OSSTART elt) 10))
         (osEnd (or (org-element-property :OSEND elt) 10))
         (link (format "%s:%s:%s:%s:%s:%s:%s:%s:%s:%s" id from to width label color fsize dir osStart osEnd))
         )
    ;; (message "debug: %s" (org-element-property :DIR elt))
    (dom-set-attribute (opm-get "itemNode" data) 'height "100%")
    (dom-set-attribute (opm-get "itemNode" data) 'width "100%")
    (dom-set-attribute (opm-get "itemNode" data) 'style "position:absolute;top:0;left:0;")
    (dom-set-attribute path 'id id)
    (dom-append-child (opm-get "itemNode" data) path)
    (if links
        (dom-set-attribute parentNode 'data-links (format "%s,%s" links link))
      (dom-set-attribute parentNode 'data-links (format "%s" link)))
    )
  )
;;|--------------------------------------------------------------
;;|Description : replace keywords in text
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 22-45-2022 12:45:40
;;|--------------------------------------------------------------
(defun opm-format (text &optional fmt)
  "replace keywords in text"
  (let* ((text (s-replace "->" "&rarr;" text))
         (text (s-replace "<-" "&larr;" text))
         (text (s-replace "¤Delta" "&Delta;" text))
         (text (s-replace "¤alpha" "&#593;" text))
         )
    (format (or fmt "%s") text)
    )
  )
;;|--------------------------------------------------------------
;;|Description : debug function
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 05-26-2019 12:26:32
;;|--------------------------------------------------------------
(defun opm-debug (elt)
  "debug"
  (message "=== got: %s" elt)
  )
(provide 'org-presentation-items)
