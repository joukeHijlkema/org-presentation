;;|--------------------------------------------------------------
;;|Description : menu for element inclusion
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-31-2021 14:31:59
;;|--------------------------------------------------------------
(defun opm-sugestion ()
  "menu for element inclusion"
  (interactive)
  (opm-insert-elt
   (ido-completing-read "insert: "
                        '("presentation"
                          "slide"
                          "box"
                          "list"
                          "text"
                          "latex"
                          "image"
                          "video"
                          "table"
                          "arrow"
                          ))
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert an element
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-28-2021 15:28:16
;;|--------------------------------------------------------------
(defun opm-insert-elt (name)
  "insert an element"
  (cond
         ((equal name "presentation") (opm-insert-params))
         ((equal name "slide") (opm-insert-slide))
         ((equal name "box") (opm-insert-box))
         ((equal name "image") (opm-insert-image))
         ((equal name "pdf") (opm-insert-pdf))
         ((equal name "arrow") (opm-insert-arrow))
         ((equal name "list") (opm-insert-list))
         ((equal name "video") (opm-insert-video))
         ((equal name "text") (opm-insert-text))
         ((equal name "latex") (opm-insert-latex))
         (t (message "Don't know what to do with %s" name))
         )
  )
;;|--------------------------------------------------------------
;;|Description : add an ellement defined by title,tag and properties
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-20-2021 10:20:05
;;|--------------------------------------------------------------
(defun opm-insert-item (tag properties &optional first)
  "add an ellement defined by title,tag and properties"
  (interactive "P")
  (if first (org-insert-heading) (org-insert-heading-after-current))
  (if (y-or-n-p "--> ?") (org-demote-subtree)
    (if (y-or-n-p "<-- ?") (org-promote-subtree))) 
  (cond ((string= tag "Slide") (insert "#Title# : #Subtitle#"))
        ((string= tag "Parameters") (insert "Parameters"))
        (t (insert (read-string "element name: "))))
  (org-set-tags (format ":%s:" tag))
  (cl-loop for (key . V1) in properties do
        (let* ((V2 (cond
                    ((string= "Choise" (car V1))
                     (ido-completing-read (nth 1 V1) (nth 2 V1)))
                    ((string= "Input" (car V1))
                     (read-string (nth 1 V1) (nth 2 V1)))
                    ((string= "List" (car V1))
                     (list (read-string (nth 1 V1) (nth 2 V1))))
                    ((string= "File" (car V1))
                     (read-file-name (nth 1 V1) (nth 2 V1) (nth 3 V1) (nth 4 V1) (nth 5 V1) ))
                    ((string= "fileRoot" (car V1))
                     (file-name-sans-extension (read-file-name (nth 1 V1) (nth 2 V1) (nth 3 V1) )))
                    ((string= "Dir" (car V1))
                     (read-directory-name (nth 1 V1) (nth 2 V1)))
                    ((string= "Fix" (car V1))
                     (nth 1 V1))
                    ((string= "newHandle" (car V1))
                     (read-string (format "handle (in use: %s): " (opm-get-handles))))
                    ((string= "Handle" (car V1))
                     (ido-completing-read (nth 1 V1) (opm-get-handles)))
                    ((string= "Style" (car V1))
                     (cl-loop for (attr . default) in (nth 1 V1)
                           concat (format "%s:%s;" attr (read-string (format "%s: " attr) default))))
                    (t (error "ORG-PRESENTATION: Don't know what to do with %s" (car V1)))
                    )
                   )
               (val (format "%s" V2))
               )
          (unless (string= "" val) (org-entry-put (point) key val))
          (when (and
               (string= tag "Slide")
               (string= key "title"))
            (beginning-of-line)
            (re-search-forward "#Title#" nil t)
            (replace-match val nil nil))
          (when (and
               (string= tag "Slide")
               (string= key "subtitle"))
            (beginning-of-line)
            (re-search-forward "#Subtitle#" nil t)
            (replace-match val nil nil))
          )
        )
  )
;;|--------------------------------------------------------------
;;|Description : insert Parameters
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-28-2021 15:28:16
;;|--------------------------------------------------------------
(defun opm-insert-params ()
  "insert a parameter block"
  (when (yes-or-no-p "realy clear the buffer ? ")
    (erase-buffer)
    (opm-insert-item
     "Parameters"
     `(("slideWidth" . ("Fix" "1400"))
       ("slideHeight" . ("Fix" "720"))
       ("xmargin" . ("Fix" "474"))
       ("ymargin" . ("Fix" "360"))
       ("logos" . ("Fix"
                   ((\"oneraLogo\" . \"\#templates\#CSS/Logos/logo_onera.svg\")
                    (\"republique\" . \"\#templates\#CSS/Logos/Republique-francaise-logo.svg\"))))
       ("backgrounds" . ("Fix"
                         ((\"bg1\" . \"\#templates\#CSS/Backgrounds/Onera_1.png\")
                          (\"bg2\" . \"\#templates\#CSS/Backgrounds/Onera_2.png\")
                          (\"slideTop\" . \"\#templates\#CSS/Backgrounds/Onera_titleTop.png\")
                          (\"slideBot\" . \"\#templates\#CSS/Backgrounds/Onera_titleBottom.png\"))))
       ("convertImages" . ("Fix" "True"))
       ("keepOldImages" . ("Fix" "False"))
       ("makePdf" . ("Fix" "False"))
       ("author" . ("Fix" "Jouke Hijlkema"))
       ("root" . ("Dir" "The root of the project: "))
       ("title" . ("Input" "Presentation title: "))
       ("subtitle" . ("Input" "Subtitle: "))
       ("footerTitle" . ("Input" "Footer title: "))
       ("footerDate" . ("Input" "Footer date: "))
       ("CSS" . ("Input" "Extra CSS: "))
       )
     t)
    )
  )
;;|--------------------------------------------------------------
;;|Description : insert a slide
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-20-2021 11:20:30
;;|--------------------------------------------------------------
(defun opm-insert-slide ()
  "insert a slide"
  (interactive)
  (opm-insert-item "Slide"
                   `(("moveX" . ("Input" "X movement (+/- x ): " "1.5"))
                     ("moveY" . ("Input" "Y movement (+/- x ): " "1.5"))
                     ("scale" . ("Input"  "scale: "))
                     ("title" . ("Input"  "title: " ,(org-entry-get (point) "TITLE" t)))
                     ("subtitle" . ("Input"  "subtitle: "))
                     ("inoutline" . ("Input" "in the outline: " "t"))
                     ("return" . ("Handle" "return to slide with handle: " nil))
                     )
                   )
  )
;;|--------------------------------------------------------------
;;|Description : insert an arrow
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-55-2021 11:55:47
;;|--------------------------------------------------------------
(defun opm-insert-arrow ()
  "insert an arrow"
  (opm-insert-item
   "arrow"
   `(("from" . ("Handle" "From: "))
     ("to" . ("Handle" "To: "))
     ("label" . ("Input" "Label: "))
     ("width" . ("Fix" "30"))
     ("color" . ("Fix" "\#0052AB"))
     ("fsize" . ("Fix" "80%"))
     ("osStart" . ("Fix" "10"))
     ("osEnd" . ("Fix" "10"))
     ("dir" . ("Input" "Direction (horizontal,verical,auto): "))
     )
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert a box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-02-2021 12:02:43
;;|--------------------------------------------------------------
(defun opm-insert-box ()
  "insert a box"
  (opm-insert-item
   "box"
   `(("title" . ("Input" "title: "))
     ("style" . ("Style" (("width" . "50%") ("font-size" . "50%"))))
     ("class" . ("Input" "Class (top,bottom,left,right,hcenter,vcenter): "))
     ("handle" . ("newHandle")))
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert a list
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-02-2021 12:02:43
;;|--------------------------------------------------------------
(defun opm-insert-list ()
  "insert a box"
  (opm-insert-item
   "blist"
   `()
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert a text item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-02-2021 12:02:43
;;|--------------------------------------------------------------
(defun opm-insert-text ()
  "insert a text item"
  (opm-insert-item
   "text"
   `()
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert an image
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-23-2021 15:23:16
;;|--------------------------------------------------------------
(defun opm-insert-image ()
  "insert an image"
  (opm-insert-item
   "image"
   `(("src" . ("File" "Src: " ,(format "%sCSS/Backgrounds" templateDir) "NYD.svg"))
     ("style" . ("Style" (("height" . "auto") ("width" . "auto")("font-size" . "50%"))))
     ("class" . ("Input" "Class (top,bottom,left,right,hcenter,vcenter): "))
     ("caption" . ("Input" "Caption: "))
     ("handle" . ("newHandle")))
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert pdf item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-04-2022 15:04:45
;;|--------------------------------------------------------------
(defun opm-insert-pdf ()
  "insert pdf item"
  (opm-insert-item
   "pdf"
   `(("src" . ("File" "Src: " ,(format "%sCSS/Backgrounds" templateDir) "NYD.svg"))
     ("style" . ("Style" (("width" . "50%") ("font-size" . "50%"))))
     ("class" . ("Input" "Class (top,bottom,left,right,hcenter,vcenter): "))
     ("caption" . ("Input" "Caption: "))
     ("handle" . ("newHandle")))
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert an image
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-23-2021 15:23:16
;;|--------------------------------------------------------------
(defun opm-insert-video ()
  "insert an video"
  (opm-insert-item
   "video"
   `(("base" . ("fileRoot" "Basename" ))
     ("exts" . ("List" "Extensions (mp4 mkv webm): "))
     ("style" . ("Style" (("width" . "50%") ("height" . "50%"))))
     )
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert latex
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-35-2022 09:35:10
;;|--------------------------------------------------------------
(defun opm-insert-latex ()
  "insert latex"
  (opm-insert-item
   "latex"
   `(("style" . ("Style" (("width" . "50%") ("font-size" . "50%"))))
      ("class" . ("Input" "Class (top,bottom,left,right,hcenter,vcenter): ")))
   )
  )
;;|--------------------------------------------------------------
;;|Description : insert a table
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-32-2023 15:32:14
;;|--------------------------------------------------------------
(defun opm-insert-table ()
  "insert a table"
  (opm-insert-item
   "table"
   `(("style" . ("Style" (("width" . "50%") ("font-size" . "50%"))))
      ("class" . ("Input" "Class (top,bottom,left,right,hcenter,vcenter): ")))
   )
  )
;;|--------------------------------------------------------------
;;|Description : create a list of all handles in the document
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 14-29-2021 12:29:29
;;|--------------------------------------------------------------
(defun opm-get-handles ()
  "create a list of all handles in the document"
  (interactive)
  (let ((candidates (org-map-entries (lambda () (org-entry-get (point) "handle")))))
    (cl-loop for c in candidates
          when c collect c into hits
          finally return (append '("") hits))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Add a position constraint
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 28-22-2022 17:22:33
;;|--------------------------------------------------------------
(defun opm-position ()
  "Add a position constraint"
  (interactive)
  (let* ((pos (ido-completing-read "method" (list "below" "between" "right" "left") nil t))
         (p1  (ido-completing-read "parent1" (opm-get-handles) nil t))
         (p2  (cond ((or (string= pos "below") (string= pos "right") (string= pos "left"))
                     (read-string "margin: " "10"))
                    ((string= pos "between")
                     (ido-completing-read "parent2" (opm-get-handles) nil t))
                    )
              )
         (p3  (read-string "center: " "0.5"))
         )
    (org-entry-put (point) "position" (format "%s:%s:%s:%s" pos p1 p2 p3))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Add a font constraint
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 28-58-2023 16:58:54
;;|--------------------------------------------------------------
(defun opm-font ()
  "Add a font constraint"
  (interactive)
  (let* ((fnt (ido-completing-read "method" (list "Latin Modern Sans" "Helvetica" "Times")))
         (code (cdr (assoc fnt '(("Latin Modern Sans" . "lmss")
                                 ("Helvetica" . "phv")
                                 ("Times" . "ptm")))))
         )
    (org-entry-put (point) "font" code)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Add a handle to the item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 28-25-2023 17:25:58
;;|--------------------------------------------------------------
(defun opm-handle ()
  "Add a handle to the item"
  (interactive)
  (let* ((handle (read-string "handle: ")))
    (if (member handle (opm-get-handles))
        (message (format "%s is already in use" handle))
      (org-entry-put (point) "handle" handle)
      )
    )
  )
(provide 'org-presentation-ui)
