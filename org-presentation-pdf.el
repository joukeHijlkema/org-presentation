;;|--------------------------------------------------------------
;;|Description : create a PDF from the presentations
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 12-23-2021 17:23:05
;;|--------------------------------------------------------------
(defun opm-runPdfTool ()
  "create a PDF from the presentations"
  (interactive)
    ;; (message "this runs the presentation tool")
    (make-local-variable 'Params)
    (setq-local imgCounter 0)
    (setq-local vidCounter 0)
    (setq-local arrowCounter 0)
    (setq-local slideCounter 0)
    (setq-local itemCounter 0)
    (setq-local X "0")
    (setq-local Y "0")
    (goto-char 0)
    (org-element-map (org-element-parse-buffer) 'headline #'opm-create-pdf-headlines)
    )
;;|--------------------------------------------------------------
;;|Description : loop over the headlines and do what is needed
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 12-38-2021 18:38:14
;;|--------------------------------------------------------------
(defun opm-create-pdf-headlines (elt)
  "loop over the headlines and do what is needed"
  (let* ((type (nth 0 (org-element-property :tags elt))))
    (cond ((equal type "Parameters") (opm-doParams elt))
          ((equal type "Slide") (opm-create-pdf-page elt))
          )
    )
  )
;;|--------------------------------------------------------------
;;|Description : transform a slide to a pdf page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 12-26-2021 17:26:37
;;|--------------------------------------------------------------
(defun opm-create-pdf-page (elt)
  "transform a slide to a pdf page"
  (let* ((title (org-element-property :ITEM elt))
         (page (svg-create (opm-getParam "SLIDEWIDTH") (opm-getParam "SLIDEHEIGHT")))
         )
    ;; (message "found title: %s" title)
    (opm-create-pdf-headers page elt)
    (with-temp-file "/tmp/page.svg"
      (set-buffer-multibyte nil)
      (svg-print page)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : do the headers and the footers
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 12-20-2021 18:20:54
;;|--------------------------------------------------------------
(defun opm-create-pdf-headers (page elt)
  "do the headers and the footers"
  (let* ((W (string-to-number (opm-getParam "SLIDEWIDTH")))
         (H (string-to-number (opm-getParam "SLIDEHEIGHT")))
         (dx 30)
         (dy 60)
         (X1 dx)
         (X2 W)
         (Y1 dy)
         (X3 dx)
         (X4 W)
         (Y2 (- H dy))
         )
  (svg-line page X1 Y1 X2 Y1 :stroke-width 1 :stroke-color "#00509A")
  (svg-line page X3 Y2 X4 Y2 :stroke-color "#00509A")
  (svg-embed page "/home/hylkema/Projects/org-presentation/CSS/Logos/Republique-francaise-logo.png"
             "image/png" nil
             :width "44px" :height "50px" :x "30px" :y "670px") 
  )
  )
;;|--------------------------------------------------------------
;;|Description : read an svg logo
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 12-10-2021 19:10:54
;;|--------------------------------------------------------------
(defun opm-create-pdf-logo (name)
  "read an svg logo"
  (interactive)
  (with-temp-buffer
    (insert-file-contents "/home/hylkema/Projects/org-presentation/CSS/Logos/Republique-francaise-logo.svg")
    (buffer-string)
    )
  )
(provide 'org-presentation-pdf)
