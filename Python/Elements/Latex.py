#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - sam. avril 13:31 2022
#   - Initial Version 1.0
#  =================================================
from .Element import Element
import cv2
import subprocess

class Latex(Element):
    def __init__(self,orgDef,slide,Params,Handles):
        "docstring"
        super(Latex, self).__init__(Handles)

        path  = "%s/IMAGES/eq_%s.svg"%(Params["root"],
                                      orgDef.get_property("id"))
        path2 = "%s/IMAGES/eq_%s.png"%(Params["root"],
                                       orgDef.get_property("id"))
        
        p=subprocess.call(["/usr/bin/convert","-density", "1200",path,"-render","-resize","600",path2])

        img = cv2.imread(path2)
        H,W,C = img.shape
        self.position(orgDef,W,H,0.9)
        
        slide.shapes.add_picture(path2,
                                 self.pos["l"],
                                 self.pos["t"],
                                 self.pos["w"],
                                 self.pos["h"])
 
