#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - ven. avril 15:11 2022
#   - Initial Version 1.0
#  =================================================

from .Element import Element
import pathlib
import cv2
import subprocess

class Image(Element):
    def __init__(self,orgDef,slide,Params,Handles):
        "docstring"
        super(Image, self).__init__(Handles)

        ext = pathlib.Path(orgDef.get_property("src")).suffix
        path = "%s/IMAGES/img_%s%s"%(Params["root"],
                                     orgDef.get_property("id"),
                                     ext)
        if ".svg" == ext:
            svg_code = open(path, 'rt').read()
            path2 = "%s/IMAGES/img_%s%s"%(Params["root"],
                                     orgDef.get_property("id"),
                                         ".png")
            p=subprocess.call(["/usr/bin/convert",path,path2])
            path = path2
                              
        img = cv2.imread(path)
        H,W,C = img.shape
        self.position(orgDef,W,H,0.9)
        
        slide.shapes.add_picture(path,
                                 self.pos["l"],
                                 self.pos["t"],
                                 self.pos["w"],
                                 self.pos["h"])
        
