#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - ven. avril 13:19 2022
#   - Initial Version 1.0
#  =================================================
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import MSO_AUTO_SIZE
from pptx.enum.text import MSO_ANCHOR
from pptx.util import Mm, Inches, Pt

class Outline():
    def __init__(self,slide,list,footerText):
        "docstring"
        super(Outline, self).__init__()

        for p in slide.shapes:
            if p.text == "Titre de la présentation":
                p.text_frame.paragraphs[0].runs[0].text = footerText
            if p.text == "#Outline#":
                p.text_frame.paragraphs[0].runs[0].text = "Outline"

        shape = slide.shapes.add_textbox(Mm(50),Mm(30),10,10)
        # shape.vertical_anchor = MSO_ANCHOR.MIDDLE
        p = None
        for i in list:
            if not p:
                p = shape.text_frame.paragraphs[0]
            else:
                p = shape.text_frame.add_paragraph()
            p.text = "- %s"%i
            shape.text_frame.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT

        
