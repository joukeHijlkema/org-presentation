#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - sam. avril 14:04 2022
#   - Initial Version 1.0
#  =================================================
from .Element import Element
from pptx.enum.shapes import MSO_SHAPE
from .Blist import Blist
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE

class Box(Element):
    def __init__(self,orgDef,slide,Params,Handles):
        "docstring"
        super(Box, self).__init__(Handles)
        print("box start")
        self.position(orgDef,1000,1000,1.0)
        box = slide.shapes.add_shape(MSO_SHAPE.ROUNDED_RECTANGLE,
                                     self.pos["l"],
                                     self.pos["t"],
                                     self.pos["w"],
                                     self.pos["h"])
        box.text_frame.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
        print(box.height)

        for i in orgDef.children:
            s = None
            ok = False
            if "blist" in i.tags:
                i.properties["style"] = orgDef.get_property("style")
                s = Blist(i,box,Params,Handles)
                box.left   = self.pos["l"]
                box.top    = self.pos["t"]
                box.width  = s.pos["w"]
                box.height = s.pos["h"]
                ok = True
            if i.get_property("handle") :
                Handles[i.get_property("handle")] = s

        print("box end")
