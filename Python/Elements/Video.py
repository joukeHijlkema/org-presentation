#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - sam. avril 12:18 2022
#   - Initial Version 1.0
#  =================================================
from .Element import Element
import pathlib
import cv2

class Video(Element):
    def __init__(self,orgDef,slide,Params,Handles):
        "docstring"
        super(Video, self).__init__(Handles)
        
        # for now we only take mp4
        if not "mp4" in orgDef.get_property("exts"):
            raise("Only MP4 videos are allowed")

        path = "%s/VIDEOS/vid_%s.mp4"%(Params["root"],
                                       orgDef.get_property("id"))
        vid = cv2.VideoCapture(path)
        H = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
        W = vid.get(cv2.CAP_PROP_FRAME_WIDTH)

        self.position(orgDef,W,H,0.9)

        slide.shapes.add_movie(path,
                               self.pos["l"],
                               self.pos["t"],
                               self.pos["w"],
                               self.pos["h"],
                               mime_type="video/mp4")
