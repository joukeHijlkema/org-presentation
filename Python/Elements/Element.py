#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - ven. avril 17:47 2022
#   - Initial Version 1.0
#  =================================================
from pptx.util import Mm,Inches,Emu,Pt

class Element():
    def __init__(self,Handles):
        "docstring"
        super(Element, self).__init__()
        self.N = Mm(23)
        self.E = Mm(252)
        self.S = Mm(128)
        self.W = Mm(8)
        self.L = self.E-self.W
        self.H = self.S-self.N

        self.pos = {}
        self.Handles = Handles
       
    ## --------------------------------------------------------------
    ## Description : postion
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-29-2022 18:29:09
    ## --------------------------------------------------------------
    def position(self,orgDef,W,H,C):
        style = self.readStyle(orgDef)
        print("dims : %sx%s -> %s"%(W,H,W/H))
        print("style: %s"%style)
        if "width" in style:
            w = C*0.01*float(style["width"].strip('%'))*self.L
            h = w*H/W
        elif "height" in style:
            h = C*0.01*float(style["height"].strip('%'))*self.H
            w = h*W/H
        else:
            h = self.H
            w = self.L

        l = self.W
        t = self.N
        if orgDef.get_property("class"):
            if "right" in orgDef.get_property("class"):
                l = self.E - w
            elif "hcenter" in orgDef.get_property("class"):
                l = 0.5*(self.E + self.W - w) 
            if "bottom" in orgDef.get_property("class"):
                t = self.S-h
            elif "vcenter" in orgDef.get_property("class"):
                t = 0.5*(self.N+self.S-h)
        elif orgDef.get_property("position"):
            elts = orgDef.get_property("position").split(":")
            print("pos : %s"%elts)
            parent = self.Handles[elts[1]]
            if "below" in elts[0]:
                m = Pt(10 if len(elts)==2 else int(elts[2]))
                t = parent.pos["t"]+parent.pos["h"]+m
                l = parent.pos["l"]+0.5*(parent.pos["w"]-w)
            elif "left" in elts[0]:
                m = Pt(10 if len(elts)==2 else int(elts[2]))
                t = parent.pos["t"]+0.5*(parent.pos["h"]-h)
                l = parent.pos["l"]-w-m
            elif "between" in elts[0]:
                parent2 = self.Handles[elts[2]]
                t = 0.5*(parent.pos["t"] +0.5*parent.pos["h"]+
                         parent2.pos["t"]+0.5*parent2.pos["h"]) - 0.5*h
                l = 0.5*(parent.pos["l"] +0.5*parent.pos["w"]+
                         parent2.pos["l"]+0.5*parent2.pos["w"]) - 0.5*w
            
        print("pptx: %s,%s,%sx%s->%s"%(Emu(l).inches,Emu(t).inches,Emu(w).inches,Emu(h).inches,w/h))
        # print("=======")
        self.pos["l"] = round(l)
        self.pos["t"] = round(t)
        self.pos["w"] = round(w)
        self.pos["h"] = round(h)
            
    ## --------------------------------------------------------------
    ## Description : readStyle
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-31-2022 18:31:36
    ## --------------------------------------------------------------
    def readStyle(self,orgDef):
        out = {}
        if orgDef.get_property("style"):
            for s in orgDef.get_property("style").split(";"):
                elt = s.split(":")
                if len(elt)==2:
                    out[elt[0]]=elt[1]
        return out
