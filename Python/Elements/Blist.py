#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - lun. mai 15:05 2022
#   - Initial Version 1.0
#  =================================================
from .Element import Element
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE
from PIL import ImageFont

class Blist(Element):
    def __init__(self,orgDef,parent,Params,Handles):
        "docstring"
        super(Blist, self).__init__(Handles)

        content = ""
        W = 0
        H = 0
        font = ImageFont.truetype('Arial.ttf', 18)
        for l in orgDef.get_body().split("\n"):
            print("#%s#"%l)
            content+=l.strip()
            size = font.getsize(l.strip())
            W = max(W,size[0])
            H += size[1]

        print("### text size %sx%s"%(W,H))
        try:
            self.position(orgDef,W,H,1.0)
            txt = parent.text_frame
        except Exception:
            txt = parent.shapes.add_textbox(self.pos["l"],
                                            self.pos["t"],
                                            self.pos["w"],
                                            self.pos["h"]).text_frame
            txt.auto_size = MSO_AUTO_SIZE.NONE
        txt.text = orgDef.get_body().strip()
        
        
