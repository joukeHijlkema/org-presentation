#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - sam. avril 12:18 2022
#   - Initial Version 1.0
#  =================================================
from .Element import Element
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.shapes import MSO_CONNECTOR
from pptx.util import Pt
from pptx.oxml import parse_xml
import math

class Arrow(Element):
    def __init__(self,orgDef,slide,Params,Handles):
        "docstring"
        super(Arrow, self).__init__(Handles)
        
        From = Handles[orgDef.get_property("from")]
        To   = Handles[orgDef.get_property("to")]

        osStart = Pt(10) if not orgDef.get_property("osStart") else Pt(int(orgDef.get_property("osStart")))
        osEnd   = Pt(10) if not orgDef.get_property("osEnd")   else Pt(int(orgDef.get_property("osEnd")))
        
        x1 = From.pos["l"]+0.5*From.pos["w"]
        y1 = From.pos["t"]+0.5*From.pos["h"]
        x2 = To.pos["l"]+0.5*To.pos["w"]
        y2 = To.pos["t"]+0.5*To.pos["h"]

        if x1==x2:
            y1 += 0.5*From.pos["h"] + osStart
            y2 -= 0.5*To.pos["h"]   + osEnd
        elif y2==y1:
            x1 += 0.5*From.pos["w"] + osStart
            x2 -= 0.5*To.pos["w"]   + osEnd
        else:

            L = math.sqrt(math.pow(x2-x1,2)+math.pow(y2-y1,2))
            
            Lambda = []
            Lambda.append((From.pos["l"]-x1)/(x2-x1))
            Lambda.append((From.pos["t"]-y1)/(y2-y1))
            Lambda.append((From.pos["l"]+From.pos["w"]-x1)/(x2-x1))
            Lambda.append((From.pos["t"]+From.pos["h"]-y1)/(y2-y1))
            Lambda.sort()

            x1 = x1 + (Lambda[2]+osStart/L)*(x2-x1)
            y1 = y1 + (Lambda[2]+osStart/L)*(y2-y1)

            Lambda = []
            Lambda.append((To.pos["l"]-x2)/(x1-x2))
            Lambda.append((To.pos["t"]-y2)/(y1-y2))
            Lambda.append((To.pos["l"]+To.pos["w"]-x2)/(x1-x2))
            Lambda.append((To.pos["t"]+To.pos["h"]-y2)/(y1-y2))
            Lambda.sort()

            x2 = x2 + (Lambda[2]+osEnd/L)*(x1-x2)
            y2 = y2 + (Lambda[2]+osEnd/L)*(y1-y2)

        if orgDef.get_property("dir"):
            if "horizontal" in orgDef.get_property("dir"):
                y2=y1
            elif "vertical" in orgDef.get_property("dir"):
                x2=x1
        
        ar = slide.shapes.add_connector(MSO_CONNECTOR.STRAIGHT,x1,y1,x2,y2)
        ar.line._get_or_add_ln().append(parse_xml("""
        <a:tailEnd type="triangle" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"/>
        """))
        width   = Pt(10) if not orgDef.get_property("width") else Pt(0.3*int(orgDef.get_property("width")))
        ar.line.width = width
