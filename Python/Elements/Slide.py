#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - ven. avril 13:28 2022
#   - Initial Version 1.0
#  =================================================

from pptx.util import Pt
from pptx.enum.text import MSO_AUTO_SIZE
from pptx.enum.text import MSO_ANCHOR
from pptx.enum.text import PP_ALIGN
from pptx.dml.color import RGBColor
from pptx.enum.shapes import MSO_SHAPE

from .Image import Image
from .Video import Video
from .Latex import Latex
from .Box import Box
from .Blist import Blist
from .Arrow import Arrow

class Slide():
    id = 3
    def __init__(self,orgDef,prs,Params):
        "docstring"
        super(Slide, self).__init__()
        
        self.slide = prs.slides.add_slide(prs.slides[2].slide_layout)

        Slide.id = Slide.id+1
        self.id = Slide.id
        self.slideNumber()
        self.slideFooters("%s : %s"%(Params["footerTitle"],Params["footerDate"]))
        self.slideTitle(orgDef.get_property("title"),orgDef.get_property("subtitle"))

        Handles = {}
        Rest = []
        for i in orgDef.children:
            s = None
            ok = False
            print("-- found: %s"%i.heading)
            print("   type : %s"%i.tags)
            if "image" in i.tags:
                s = Image(i,self.slide,Params,Handles)
                ok = True
            elif "video" in i.tags:
                s = Video(i,self.slide,Params,Handles)
                ok = True
            elif "latex" in i.tags:
                s = Latex(i,self.slide,Params,Handles)
                ok = True
            elif "box" in i.tags:
                s = Box(i,self.slide,Params,Handles)
                ok = True
            elif "blist" in i.tags:
                s = Blist(i,self.slide,Params,Handles)
                ok = True
            elif "arrow" in i.tags:
                s = Arrow(i,self.slide,Params,Handles)
                ok = True
            if i.get_property("handle") :
                Handles[i.get_property("handle")] = s
            print("   OK   : %s"%ok)
            if not ok:
                Rest.append(i.tags)
        print("Left to do:")
        for r in Rest:
            print("- %s"%r)
               
    ## --------------------------------------------------------------
    ## Description : slide numbers
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-26-2022 14:26:13
    ## --------------------------------------------------------------
    def slideNumber(self):
        shape = self.slide.shapes.add_textbox(Pt(657),Pt(365),Pt(60),Pt(40))
        shape.text_frame.auto_size = MSO_AUTO_SIZE.NONE
        shape.text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
        par = shape.text_frame.paragraphs[0]
        par.alignment = PP_ALIGN.CENTER
        par.text = "%s"%self.id
        par.font.size = Pt(12)
      

    ## --------------------------------------------------------------
    ## Description : footers
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-29-2022 14:29:14
    ## --------------------------------------------------------------
    def slideFooters(self,text):
        shape = self.slide.shapes.add_textbox(Pt(224),Pt(365),Pt(442),Pt(40))
        shape.text_frame.auto_size = MSO_AUTO_SIZE.NONE
        shape.text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
        par = shape.text_frame.paragraphs[0]
        par.alignment = PP_ALIGN.RIGHT
        par.text = text
        par.font.size = Pt(10)
        par.font.color.rgb = RGBColor(139,139,139) 
        
    ## --------------------------------------------------------------
    ## Description : Title
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-43-2022 14:43:45
    ## --------------------------------------------------------------
    def slideTitle(self,title,subtitle=None):
        shape = self.slide.shapes.add_textbox(Pt(21),Pt(0),Pt(690),Pt(60))
        shape.text_frame.auto_size = MSO_AUTO_SIZE.NONE
        shape.text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
                        
        par = shape.text_frame.paragraphs[0]
        par.alignment = PP_ALIGN.LEFT
        par.text = title
        par.font.size = Pt(24)
        par.font.bold = True
        par.font.color.rgb = RGBColor(0,80,154) 
        
        if subtitle:
            par = shape.text_frame.add_paragraph()
            par.alignment = PP_ALIGN.LEFT
            par.text = subtitle
            par.font.size = Pt(18)
            par.font.color.rgb = RGBColor(0,80,154) 
