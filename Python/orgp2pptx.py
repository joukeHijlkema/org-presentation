#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - jeu. avril 17:48 2022
#   - Initial Version 1.0
#  =================================================
import argparse
from orgparse import load, loads
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import MSO_AUTO_SIZE
from pptx.enum.text import MSO_ANCHOR
from pptx.util import Inches, Pt

from Elements import *

parser = argparse.ArgumentParser()
parser.add_argument("--input",help="input orgp file",type=str)
args = parser.parse_args()

root = load(args.input)

Params = {}
OutlineList = []
Slides = []
for s in root.children:
    if s.heading == "Parameters":
        for p in s.properties:
            Params[p] = s.properties[p]
    if s.get_property("inoutline"):
        OutlineList.append(s.get_property("title"))
    if "Slide" in s.tags and not s.get_property("outline"):
        Slides.append(s)
        
prs = Presentation("/home/hylkema/myConfigs/Emacs/straight/repos/org-presentation-mode/Python/Template.pptx")

# Title slide
for p in prs.slides[1].shapes:
    if p.text == "#Title#":
        p.text_frame.paragraphs[0].runs[0].text = Params["title"]
    if p.text == "#Author#":
        p.text_frame.paragraphs[0].runs[0].text = Params["author"]
# Outline
Outline.Outline(prs.slides[2],OutlineList,"%s : %s"%(Params["footerTitle"],Params["footerDate"]))
#Slides
for s in Slides:
    print("=== SLIDE: %s"%s.heading)
    Slide.Slide(s,prs,Params)

prs.save("presentation.pptx")
