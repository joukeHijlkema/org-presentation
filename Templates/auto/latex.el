(TeX-add-style-hook
 "latex"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "multi={mymath}" "border=1pt")))
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "amsmath")
   (LaTeX-add-environments
    "mymath"))
 :latex)

