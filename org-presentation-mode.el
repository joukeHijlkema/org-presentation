(define-derived-mode org-presentation-mode org-mode "org-presentation mode"
  "a variant of org-mode that produces HTML/impress.js presentations from an org file.
   the document structure is important:
   * Parameters
     the propertie drawer contains the parameters for this presentation
   * slide 1 :Slide:
   ...
   * slide n :Slide:

   Remarks:
   * Latex
     - \\begin{ causes trouble. Use \\beg{, the tool does a substitution
   "
  (require 'esxml)
  (require 'dom)
  (require 'easymenu)
  (require 'org-presentation-main)
  (require 'org-presentation-items)
  (require 'org-presentation-ui)
  (setq debug-on-error t)
  ;; (setq debug-on-signal nil)
  ;; === Menu ===
  ;; use C-x 8 RET to insert special characters
  (easy-menu-define org-presentation-menu org-presentation-mode-map "Org presentation menu"
    '("Org presentations"
      ("Insert"
       ["Presentation" (opm-insert-params) t]
       ["Slide" (opm-insert-slide) t]
       ["Box" (opm-insert-box) t]
       ["List" (opm-insert-list) t]
       ["Text" (opm-insert-text) t]
       ["Latex" (opm-insert-latex) t]
       ["Image" (opm-insert-image) t]
       ["Video" (opm-insert-video) t]
       ["Table" (opm-insert-table) t]
       ["Arrow" (opm-insert-arrow) t]
       )
      ("Constraints"
       ["position" (opm-position) t]
       ["font" (opm-font) t]
       ["handle" (opm-handle) t]
       )
      ("Special charcater"
       ["ṁ" (insert "&#x1E41") t]
       ["Δ" (insert "&Delta;") t]
       ["←" (insert "&lArr;") t]
       ["→" (insert "&rArr;") t]
       )
      )
    )
  :after-hook (load-file (locate-file "org-presentation-main.el" load-path))
  :after-hook (load-file (locate-file "org-presentation-items.el" load-path))
  :after-hook (load-file (locate-file "org-presentation-ui.el" load-path))
  
  (setq debug-on-error t)
  (setq debug-on-signal nil)
)

(provide 'org-presentation-mode)
